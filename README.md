### 1. Find out descriptions for:

*  _sampleDynamicPivotGridCtrl.toggleIsEnabledWhatIfAnalysis_

* _sampleDynamicPivotGridCtrl.toggleUseChangesCache_

* _sampleDynamicPivotGridCtrl.recalculate_

* _sampleDynamicPivotGridCtrl.save_

* _sampleDynamicPivotGridCtrl.rollback_

* _sampleDynamicPivotGridCtrl.clearLocalChanges_

* _sampleDynamicPivotGridCtrl.removeAllActions_

* _cellConditionsDesignerCtrl.isVisibleSecondTextBox_

* _cellConditionsDesignerCtrl.cubeMetaData_

* _cellConditionsDesignerCtrl.arrayDescriptors_

* _cellConditionsDesignerCtrl.openGroups_

* _cellConditionsDesignerCtrl.onMemberUniqueChanged_

* _cellConditionsDesignerCtrl.getImageDescription_

* _cellConditionsDesignerCtrl.setImageCondition_

* _cellConditionsDesignerCtrl.cancel_

* _cellConditionsDesignerCtrl.ok_

* _editPageCtrl.isEnabledEditCube_

* _showMdxQueryCtrl_

* _messageServiceCtrl_

* _customCalculationsEditorCtrl_

* _cubeChoiceControl_ directive

* factories 

### 2. Desribe listeners in _sampleDynamicPivotGridCtrl_

### 3. Find out used type in: 

   * _cellConditionsDesignerCtrl.setColors_

### 4. If nesessary describe every type used in controllers

####       Currently undefined types:
######     * _cellConditionsDesignerCtrl.setColors_
######     * _cellConditionsDesignerCtrl.typeConditions_
######     * _sampleDynamicPivotGridCtrl.onCreateRefControl_
######     * _sampleDynamicPivotGridCtrl.onCreateRefControl_
######     * _Page_ object in _sampleDynamicPivotGridCtrl_
######     * _cellConditionsDesignerCtrl.arrayDescriptors_
######     * _cellConditionsDesignerCtrl.openGroups_
######     * _cellConditionsDesignerCtrl.setCurrentDescriptor_
######     * _cellConditionsDesignerCtrl.setCondition_
######     * _cellConditionsDesignerCtrl.getBackColorDescription_
######     * _cellConditionsDesignerCtrl.getImageDescription_
######     * objects used in **Factories**